# Derplang

## what is derplang?
this is a old program I made back when I was a student.
Orginally the program was written in java and now ported to c#.
what this program does is removing litterals but for every litteral
it checks if a condition is met if not the litteral stays,
this result in very weird texts.

please do not confuse this program with a programming language these are both
completely different things!.

## can derplang do more?
The awnser is yes, derplang has multiple filters aswell.
this includes breezah, l33t, demon talk (making text backwards), caps and
change order.

## why was it made?
orginally it was made to sound kinda drunk and annoying, I wanted to 
type ban appeals with it.
however after I met some people with dyslexia which called me out
that I was making fun of them I decided to stop using it.

## why you still release this software?
to be honest some of my friends actually got dyslexia.
and I asked them if this could simulate the way how somebody with dyslexia could
read and if they would be able to read better or worse.
I got mixed responses really, and for that reason I believe this program should
be released for science based and or educational concepts only.

however I still think this program can be used to troll...
but don’t use it on people with dyslexia that is just not cool!.
I rather copy a copyright license and convert it and have a good laugh at it.
if I notice people abusing this software to bully others I might delete the repo.