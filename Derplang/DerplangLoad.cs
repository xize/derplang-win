﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Windows.Forms;

namespace Derplang
{
    public partial class DerplangLoad : Form
    {
        private Timer t = new Timer();
        private Timer t2 = new Timer();

        public DerplangLoad()
        {
            InitializeComponent();
        }

        private void DerplangLoad_Load(object sender, EventArgs e)
        {
            this.Opacity = 0;
            t.Interval = 10;
            t.Tick += new EventHandler(FadeIn);
            t.Start();
        }

        private void FadeIn(object sender, EventArgs e)
        {

            if (this.Opacity >= 1)
            {
                t.Stop();
                //start the progress bar...
                t2.Interval = 500;
                t2.Tick += new EventHandler(tickProgress);
                t2.Start();
            }
            else
            {
                this.Opacity += 0.01;
            }
        }

        private void tickProgress(object sender, EventArgs e)
        {

            if (this.progressBar1.Value >= 100)
            {
                t2.Stop();
                this.Hide();
                Window wind = new Window(this);
                wind.Show();
            }
            else
            {
                Random rand = new Random();
                int randomval = rand.Next(1, 25);

                if (this.progressBar1.Value == 0)
                {
                    label1.Text = "loading please wait... 0%";
                    label1.Update();
                }

                if (randomval + this.progressBar1.Value <= 100)
                {
                    progressBar1.Value += randomval;
                    label1.Text = "loading please wait... "+progressBar1.Value+"%";
                    label1.Update();
                }
                else if (randomval + this.progressBar1.Value > 100)
                {
                    progressBar1.Value = 100;
                    label1.Text = "loading please wait... " + progressBar1.Value + "%";
                    label1.Update();
                }
            }
        }
    }
}
