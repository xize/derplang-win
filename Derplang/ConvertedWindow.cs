﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Derplang
{
    public partial class ConvertedWindow : Form
    {
        public ConvertedWindow()
        {
            InitializeComponent();
        }

        private void Copybtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("the text has been copied! :)", "converted text copied!");
            Clipboard.SetText(this.textbox.Text);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //first reset our own textbox
            this.textbox.Text = "";

            //first get the MDI parent :)
            Form f = this.MdiParent;

            //now get the mdi child from the parent form
            foreach(Form child in f.MdiChildren)
            {
                if(child is TextWindow)
                {
                    TextWindow textwindow = (TextWindow)child;
                    textwindow.textbox.Text = "";
                    break;
                }
            }
            MessageBox.Show("The Text has been successfully reset", "text reset");
        }

        private void Convertbtn_Click(object sender, EventArgs e)
        {

            Util u = new Util((Window)this.MdiParent);

            u.ConvertText();

        }
    }
}
