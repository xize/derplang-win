﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Derplang
{
    public partial class Window : Form
    {

        private TextWindow textwindow;
        private ConvertedWindow convertedwindow;
        private OptionsWindow optionswindow;
        private SpeechWindow speechwindow;

        private String windowtitle = "Derplang {0} Alpha";

        private DerplangLoad splashscreen;

        public Window(DerplangLoad splashscreen)
        {
            this.splashscreen = splashscreen;
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
          
            this.Text = String.Format(windowtitle, Application.ProductVersion.Substring(0, 3));
            this.FormClosing += new FormClosingEventHandler(CleanupOnExit);

            TextWindow text = new TextWindow();
            text.MdiParent = this;
            text.Show();
            this.textwindow = text;

            ConvertedWindow converted = new ConvertedWindow();
            converted.MdiParent = this;
            converted.Show();
            converted.Location = new Point(278, 0);
            this.convertedwindow = converted;

            OptionsWindow optionwindow = new OptionsWindow();
            optionwindow.MdiParent = this;
            optionwindow.Show();
            optionwindow.Location = new Point(562, 20);
            this.optionswindow = optionwindow;

            SpeechWindow speechwindow = new SpeechWindow();
            speechwindow.MdiParent = this;
            speechwindow.Show();
            speechwindow.Location = new Point(0, 262);
            this.speechwindow = speechwindow;

            this.optiontextwindow.Checked = true;
            this.optionconvertwindow.Checked = true;
            this.optiontoolswindow.Checked = true;
            this.optionspeechwindow.Checked = true;

            foreach (Form mdiform in this.MdiChildren)
            {
                mdiform.FormClosing += new FormClosingEventHandler(CloseWindow);
            }
        }

        private void CleanupOnExit(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.ApplicationExitCall)
            {
                foreach (Form mdiform in this.MdiChildren)
                {
                    mdiform.MdiParent = null;
                }
            }
            this.splashscreen.Close();
        }

        public void CloseWindow(Object sender, FormClosingEventArgs args)
        {
            if (args.CloseReason == CloseReason.UserClosing)
            {
                if (sender is Form)
                {
                    Form f = (Form)sender;
                    f.Hide();

                    if(f is TextWindow)
                    {
                        this.optiontextwindow.Checked = false;
                    } else if(f is ConvertedWindow)
                    {
                        this.optionconvertwindow.Checked = false;
                    } else if(f is OptionsWindow)
                    {
                        this.optiontoolswindow.Checked = false;
                    } else if(f is SpeechWindow)
                    {
                        this.optionspeechwindow.Checked = false;
                    }

                    args.Cancel = true;
                }
            }
        }

        private void ShowTextWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.optiontextwindow.Checked = true;
            this.textwindow.Show();
            this.textwindow.Location = new Point(0, 0);
        }

        private void ShowConvertWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.optionconvertwindow.Checked = true;
            this.convertedwindow.Show();
            this.convertedwindow.Location = new Point(278, 0);
        }

        private void ShowToolsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.optiontoolswindow.Checked = true;
            this.optionswindow.Show();
            this.optionswindow.Location = new Point(562, 20);
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openfile.ShowDialog();
        }

        private void Openfile_FileOk(object sender, CancelEventArgs e)
        {
            if(openfile.FileName != "OpenFile" && openfile.CheckFileExists)
            {
                String text = File.ReadAllText(openfile.FileName);
                this.Text = String.Format(this.windowtitle, Application.ProductVersion.Substring(0, 3))+" opening file: "+openfile.SafeFileName;
                this.textwindow.textbox.Text = text;
            }
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openfile.FileName != "OpenFile" && openfile.CheckFileExists)
            {
                if (openfile.SafeFileName.EndsWith(".wav"))
                {
                    this.speechwindow.speech.SpeakAsyncCancelAll();
                    this.speechwindow.speech.SetOutputToWaveFile(openfile.FileName);
                    this.speechwindow.speech.Volume = this.speechwindow.getVolume();
                    this.speechwindow.speech.Rate = this.speechwindow.getSpeed();
                    this.speechwindow.speech.Speak(this.convertedwindow.textbox.Text);
                    this.speechwindow.speech.SetOutputToDefaultAudioDevice();
                }
                else
                {
                    File.WriteAllText(openfile.FileName, this.convertedwindow.textbox.Text);
                    MessageBox.Show(openfile.FileName + " has been saved!", "the derp text has been saved!");
                }
            }
        }

        private void FileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openfile.FileName != "OpenFile")
            {
                this.saveToolStripMenuItem.Enabled = true;
            } else
            {
                this.saveToolStripMenuItem.Enabled = false;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.savefileas.ShowDialog();
        }

        private void Savefileas_FileOk(object sender, CancelEventArgs e)
        {
            this.openfile.FileName = this.savefileas.FileName;
            this.Text = String.Format(this.windowtitle, Application.ProductVersion.Substring(0, 3)) + " opening file: " + openfile.SafeFileName;
            if (this.openfile.SafeFileName.EndsWith(".wav"))
            {
                this.speechwindow.speech.SpeakAsyncCancelAll();
                this.speechwindow.speech.SetOutputToWaveFile(this.savefileas.FileName);
                this.speechwindow.speech.Volume = this.speechwindow.getVolume();
                this.speechwindow.speech.Rate = this.speechwindow.getSpeed();
                this.speechwindow.speech.Speak(this.convertedwindow.textbox.Text);
                this.speechwindow.speech.SetOutputToDefaultAudioDevice();
                MessageBox.Show("the audio has been saved!", "success!");
            }
            else
            {
                File.WriteAllText(this.savefileas.FileName, this.convertedwindow.textbox.Text);
            }
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutWindow about = new AboutWindow();
            about.ShowDialog();
        }

        private void LicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LicenseWindow licensewind = new LicenseWindow();
            licensewind.ShowDialog();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.splashscreen.Close();
        }

        private void ShowSpeechhWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.optionspeechwindow.Checked = true;
            this.speechwindow.Show();
            speechwindow.Location = new Point(0, 262);
        }
    }
}
