﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using Timer = System.Threading.Timer;

namespace Derplang
{
    class Util
    {

        private Window win;
        private TextWindow textwindow;
        private ConvertedWindow convertedwindow;
        private OptionsWindow optionwindw;
        private MdiClient mclient;
        private BussyWindow bussy = new BussyWindow();

        private CancellationToken token;

        public Util(Window win)
        {
            this.win = win;
            foreach (Form f in win.MdiChildren)
            {
                if(f is OptionsWindow)
                {
                    this.optionwindw = (OptionsWindow)f;
                } else if(f is TextWindow)
                {
                    this.textwindow = (TextWindow)f;
                } else if(f is ConvertedWindow)
                {
                    this.convertedwindow = (ConvertedWindow) f;
                }
            }

            foreach (Control c in win.Controls)
            {
                if(c is MdiClient)
                {
                    this.mclient = (MdiClient)c;
                    break;
                }
            }

        }

        /**
	 * @author xize
	 * @param message - converts a normal message in a derpish message :)
	 * @return String
	 */
        private String GetDrunkMessageFrom(String message, bool bol)
        {
            bool posneg = bol;
            bool isSpace = false;
            String newmsg = "";

            foreach(char chr in message.ToCharArray())
            {
                if (IsLitteral(chr))
                {
                    if (posneg)
                    {
                        //don't add chr, only make sure the next char will be accepted.
                        posneg = false;
                    }
                    else
                    {
                        newmsg += chr;
                        posneg = true;
                    }
                }
                else
                {
                    if (isSpace)
                    {
                        if (!IsSpace(chr))
                        {
                            isSpace = false;
                            newmsg += chr;
                        }
                    }
                    else
                    {
                        if (IsSpace(chr))
                        {
                            isSpace = true;
                            newmsg += chr;
                        }
                        else
                        {
                            newmsg += chr;
                            isSpace = false;
                        }
                    }
                }
            }
            return newmsg;
        }

        /**
         * @author xize
         * @param message - sets it to breezah
         * @param bol - boolean
         * @return String
         */
        private String ToBreeza(String message, bool bol)
        {
            bool posneg = bol;
            String newmsg = "";
            foreach(char chr in message.ToCharArray())
            {
                if (posneg)
                {
                    newmsg += ("" + chr).ToUpper();
                    posneg = false;
                }
                else
                {
                    newmsg += ("" + chr).ToLower();
                    posneg = true;
                }
            }
            return newmsg;
        }

        /**
         * @author xize
         * @param returns the demon lang
         * @param s - the message
         * @return String
         */
        private String ToDemon(String s)
        {
            String message = "";
            char[] chrs = s.ToCharArray();
            for (int i = (chrs.Length - 1); i > -1; i--)
            {
                message += chrs[i];
            }
            return message;
        }

        /**
         * @author xize
         * @param s - the message
         * @return convert the message into l33t
         */
        private String ToLeet(String s)
        {
            String msg = "";
            foreach (char chr in s.ToCharArray())
            {
                msg += "" + ConvertToLeet(chr);
            }
            return msg;
        }

        private bool IsSpace(char chr)
        {
            switch (chr)
            {
                case ' ': return true;
                default: return false;
            }
        }

        private bool IsLitteral(char chr)
        {
            switch (chr)
            {
                case 'a': return true;
                case 'e': return true;
                case 'i': return true;
                case 'o': return true;
                case 'u': return true;
                case 'A': return true;
                case 'E': return true;
                case 'I': return true;
                case 'O': return true;
                case 'U': return true;
                default: return false;
            }
        }

        private char ConvertToLeet(char chr)
        {
            switch (chr)
            {
                case 'a': return '4';
                case 'e': return '3';
                case 'i': return '1';
                case 'o': return '0';
                case 'u': return 'u';
                case 'A': return '4';
                case 'E': return '3';
                case 'I': return '1';
                case 'O': return '0';
                case 'U': return 'U';
                default: return chr;
            }
        }

        public void ConvertText()
        {

            String text = this.textwindow.textbox.Text;

            if(text.Split('\n').Length > 500)
            {
                //we go async
                this.mclient.Enabled = false;

                bussy.Show();
                bussy.Update();

                Task<String> converted = ConvertAsync(text, this.optionwindw.derp.Checked, this.optionwindw.breezahcheckbox.Checked, this.optionwindw.leetcheckbox.Checked, optionwindw.demontalkcheckbox.Checked, optionwindw.capscheckbox.Checked);

                converted.Wait();

                bussy.Close();
                this.mclient.Enabled = true;
                text = converted.Result;
            } else
            {
                //we keep it more relax.
                text = Convert(text, this.optionwindw.derp.Checked, this.optionwindw.breezahcheckbox.Checked, this.optionwindw.leetcheckbox.Checked, this.optionwindw.demontalkcheckbox.Checked, optionwindw.capscheckbox.Checked);
            }

            this.convertedwindow.textbox.Text = text;

        }

        public async Task<String> ConvertAsync(String normaltext, bool isderp, bool isbreezah, bool leetspeak, bool demon, bool caps)
        {
            String text = normaltext;

            this.win.Invoke((MethodInvoker)delegate ()
            {
                this.win.statuslabel.Text = "bussy";
            });
            

            if (isderp)
            {
                this.win.Invoke((MethodInvoker)delegate ()
                {
                    this.win.statusprogress.Value += 10;
                    this.win.statusStrip1.Update();
                });
                
                text = this.GetDrunkMessageFrom(text, isderp);
                
            }

            if (isbreezah)
            {
                this.win.Invoke((MethodInvoker)delegate ()
                {
                    this.win.statusprogress.Value += 10;
                    this.win.statusStrip1.Update();
                });
                
                text = this.ToBreeza(text, isbreezah);
                
            }
            if (leetspeak)
            {
                this.win.Invoke((MethodInvoker)delegate ()
                {
                    this.win.statusprogress.Value += 10;
                    this.win.statusStrip1.Update();
                });
                
                text = this.ToLeet(text);
                
            }
            if (demon)
            {
                this.win.Invoke((MethodInvoker)delegate ()
                {
                    this.win.statusprogress.Value += 10;
                    this.win.statusStrip1.Update();
                });
                
                text = this.ToDemon(text);
                
            }
            if (caps)
            {
                this.win.Invoke((MethodInvoker)delegate ()
                {
                    this.win.statusprogress.Value += 10;
                    this.win.statusStrip1.Update();
                });
                
                text = text.ToUpper();
                
            }
            this.win.Invoke((MethodInvoker)delegate ()
            {
                this.win.statusprogress.Value = 100;
                Thread.Sleep(100);
                this.win.statusprogress.Value = 0;
                this.win.statuslabel.Text = "idle";
                this.win.statusStrip1.Update();
            });

            return text;
        }

        public String Convert(String normaltext, bool isderp, bool isbreezah, bool leetspeak, bool demon, bool caps)
        {
            String text = normaltext;

            this.win.statuslabel.Text = "bussy";

            if (isderp)
            {
                this.win.statusprogress.Value += 10;
                text = this.GetDrunkMessageFrom(text, isderp);
                this.win.statusStrip1.Update();
            }

            if (isbreezah)
            {
                this.win.statusprogress.Value += 10;
                text = this.ToBreeza(text, isbreezah);
                this.win.statusStrip1.Update();
            }
            if (leetspeak)
            {
                this.win.statusprogress.Value += 10;
                text = this.ToLeet(text);
                this.win.statusStrip1.Update();
            }
            if (demon)
            {
                this.win.statusprogress.Value += 10;
                text = this.ToDemon(text);
                this.win.statusStrip1.Update();
            }
            if (caps)
            {
                this.win.statusprogress.Value += 10;
                text = text.ToUpper();
                this.win.statusStrip1.Update();
            }

            this.win.statusprogress.Value = 100;
            Thread.Sleep(100);
            this.win.statusprogress.Value = 0;
            this.win.statuslabel.Text = "idle";
            this.win.statusStrip1.Update();

            return text;
        }
    }
}
