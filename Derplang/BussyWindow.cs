﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derplang
{
    public partial class BussyWindow : Form
    {

        public BussyWindow()
        {
            InitializeComponent();
        }

        private void BussyWindow_Load(object sender, EventArgs e)
        {
            this.FormClosing += new FormClosingEventHandler(onCloseEv);
        }

        private void onCloseEv(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
