﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Derplang
{
    public partial class TextWindow : Form
    {
        public TextWindow()
        {
            InitializeComponent();
        }

        private void TextWindow_Load(object sender, EventArgs e)
        {
            this.textbox.TextChanged += new EventHandler(updateText);
        }

        private void updateText(object sender, EventArgs e)
        {
            Util u = new Util((Window)this.MdiParent);
            u.ConvertText();
        }
    }
}
