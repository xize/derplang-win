﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Windows.Forms;

namespace Derplang
{
    public partial class SpeechWindow : Form
    {

        private bool isSpeaking = false;
        private String speakdata = "";
        public SpeechSynthesizer speech = new SpeechSynthesizer();

        public SpeechWindow()
        {
            InitializeComponent();
        }

        public int getVolume()
        {
            Control[] controls = this.volumegroup.Controls.Find("volumetrack", true);
            foreach(Control c in controls)
            {
                if(c is TrackBar)
                {
                    TrackBar track = (TrackBar)c;
                    return track.Value;
                }
            }
            return 0;
        }

        public int getSpeed()
        {
            Control[] controls = this.speedgroup.Controls.Find("speedtrack", true);
            foreach (Control c in controls)
            {
                if (c is TrackBar)
                {
                    TrackBar track = (TrackBar)c;
                    return track.Value;
                }
            }
            return 0;
        }

        private void Playnormalbtn_Click(object sender, EventArgs e)
        {
            if (this.isSpeaking)
            {
                if(this.isSpeaking)
                {
                    speech.SpeakAsyncCancelAll();
                    this.isSpeaking = !this.isSpeaking;
                }
            }

            //first get the parent form ;-)
            Window wdw = (Window)this.MdiParent;
            //now get all the mdi controls.
            TextWindow textwindow = null;
            foreach (Form f in wdw.MdiChildren)
            {
                if (f is TextWindow)
                {
                    textwindow = (TextWindow)f;
                    break;
                }
            }
            this.isSpeaking = true;
            this.speech.Volume = this.getVolume();
            this.speech.Rate = this.getSpeed();
            this.speakdata = textwindow.textbox.Text;
            speech.SpeakAsync(this.speakdata);
            speech.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(speechcomplete);
            speech.SpeakProgress += new EventHandler<SpeakProgressEventArgs>(speechprogress);
        }

        private void speechprogress(object sender, SpeakProgressEventArgs e)
        {
            if (speech.Volume != this.getVolume() || speech.Rate != this.getSpeed())
            {
                speech.Volume = this.getVolume();
                speech.Rate = this.getSpeed();
                this.speakdata = speakdata.Remove(0, e.CharacterPosition);
                speech.SpeakAsyncCancelAll();
                speech.SpeakAsync(this.speakdata);
            }
        }

        private void speechcomplete(object sender, SpeakCompletedEventArgs e)
        {
            this.isSpeaking = false;
        }

        private void SpeechWindow_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.speech.SpeakAsyncCancelAll();
        }

        private void Playderpbtn_Click(object sender, EventArgs e)
        {
            if (this.isSpeaking)
            {
                if (this.isSpeaking)
                {
                    speech.SpeakAsyncCancelAll();
                    this.isSpeaking = !this.isSpeaking;
                }
            }

            //first get the parent form ;-)
            Window wdw = (Window)this.MdiParent;
            //now get all the mdi controls.
            ConvertedWindow convertedwindow = null;
            foreach (Form f in wdw.MdiChildren)
            {
                if (f is ConvertedWindow)
                {
                    convertedwindow = (ConvertedWindow)f;
                    break;
                }
            }
            this.isSpeaking = true;
            this.speech.Volume = this.getVolume();
            this.speech.Rate = this.getSpeed();
            this.speakdata = convertedwindow.textbox.Text;
            speech.SpeakAsync(this.speakdata);
            speech.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(speechcomplete);
            speech.SpeakProgress += new EventHandler<SpeakProgressEventArgs>(speechprogress);
        }
    }
}
