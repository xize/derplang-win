﻿namespace Derplang
{
    partial class SpeechWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpeechWindow));
            this.volumegroup = new System.Windows.Forms.GroupBox();
            this.volumetrack = new System.Windows.Forms.TrackBar();
            this.playnormalbtn = new System.Windows.Forms.Button();
            this.playderpbtn = new System.Windows.Forms.Button();
            this.speedgroup = new System.Windows.Forms.GroupBox();
            this.speedtrack = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.volumegroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumetrack)).BeginInit();
            this.speedgroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedtrack)).BeginInit();
            this.SuspendLayout();
            // 
            // volumegroup
            // 
            this.volumegroup.Controls.Add(this.volumetrack);
            this.volumegroup.Location = new System.Drawing.Point(12, 12);
            this.volumegroup.Name = "volumegroup";
            this.volumegroup.Size = new System.Drawing.Size(320, 67);
            this.volumegroup.TabIndex = 0;
            this.volumegroup.TabStop = false;
            this.volumegroup.Text = "Volume:";
            // 
            // volumetrack
            // 
            this.volumetrack.LargeChange = 1;
            this.volumetrack.Location = new System.Drawing.Point(6, 19);
            this.volumetrack.Maximum = 100;
            this.volumetrack.Minimum = 1;
            this.volumetrack.Name = "volumetrack";
            this.volumetrack.Size = new System.Drawing.Size(308, 45);
            this.volumetrack.TabIndex = 1;
            this.volumetrack.Value = 50;
            // 
            // playnormalbtn
            // 
            this.playnormalbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.playnormalbtn.Location = new System.Drawing.Point(94, 168);
            this.playnormalbtn.Name = "playnormalbtn";
            this.playnormalbtn.Size = new System.Drawing.Size(114, 23);
            this.playnormalbtn.TabIndex = 1;
            this.playnormalbtn.Text = "play normal speech";
            this.playnormalbtn.UseVisualStyleBackColor = true;
            this.playnormalbtn.Click += new System.EventHandler(this.Playnormalbtn_Click);
            // 
            // playderpbtn
            // 
            this.playderpbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.playderpbtn.Location = new System.Drawing.Point(214, 168);
            this.playderpbtn.Name = "playderpbtn";
            this.playderpbtn.Size = new System.Drawing.Size(114, 23);
            this.playderpbtn.TabIndex = 2;
            this.playderpbtn.Text = "play derp speech";
            this.playderpbtn.UseVisualStyleBackColor = true;
            this.playderpbtn.Click += new System.EventHandler(this.Playderpbtn_Click);
            // 
            // speedgroup
            // 
            this.speedgroup.Controls.Add(this.speedtrack);
            this.speedgroup.Location = new System.Drawing.Point(12, 85);
            this.speedgroup.Name = "speedgroup";
            this.speedgroup.Size = new System.Drawing.Size(320, 67);
            this.speedgroup.TabIndex = 1;
            this.speedgroup.TabStop = false;
            this.speedgroup.Text = "Speed:";
            // 
            // speedtrack
            // 
            this.speedtrack.LargeChange = 1;
            this.speedtrack.Location = new System.Drawing.Point(6, 19);
            this.speedtrack.Minimum = -10;
            this.speedtrack.Name = "speedtrack";
            this.speedtrack.Size = new System.Drawing.Size(308, 45);
            this.speedtrack.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(12, 168);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "stop";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // SpeechWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 203);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.speedgroup);
            this.Controls.Add(this.playderpbtn);
            this.Controls.Add(this.playnormalbtn);
            this.Controls.Add(this.volumegroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(356, 242);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(356, 242);
            this.Name = "SpeechWindow";
            this.Text = "Speech:";
            this.Load += new System.EventHandler(this.SpeechWindow_Load);
            this.volumegroup.ResumeLayout(false);
            this.volumegroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumetrack)).EndInit();
            this.speedgroup.ResumeLayout(false);
            this.speedgroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedtrack)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox volumegroup;
        private System.Windows.Forms.TrackBar volumetrack;
        private System.Windows.Forms.Button playnormalbtn;
        private System.Windows.Forms.Button playderpbtn;
        private System.Windows.Forms.GroupBox speedgroup;
        private System.Windows.Forms.TrackBar speedtrack;
        private System.Windows.Forms.Button button1;
    }
}