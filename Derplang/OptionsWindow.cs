﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Derplang
{
    public partial class OptionsWindow : Form
    {
        public OptionsWindow()
        {
            InitializeComponent();
        }

        private void OptionsWindow_Load(object sender, EventArgs e)
        {
            foreach(Control c in this.Controls)
            {
                if(c is CheckBox)
                {
                    CheckBox check = (CheckBox)c;
                    check.CheckedChanged += new EventHandler(UpdateForms);
                }
            }
        }

        private void UpdateForms(object sender, EventArgs e)
        {
            Util u = new Util((Window)this.MdiParent);
            u.ConvertText();
        }
    }
}
