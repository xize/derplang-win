﻿namespace Derplang
{
    partial class OptionsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.breezahcheckbox = new System.Windows.Forms.CheckBox();
            this.leetcheckbox = new System.Windows.Forms.CheckBox();
            this.demontalkcheckbox = new System.Windows.Forms.CheckBox();
            this.capscheckbox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.overridekeyboardcheckbox = new System.Windows.Forms.CheckBox();
            this.changeordercheckbox = new System.Windows.Forms.CheckBox();
            this.derp = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // breezahcheckbox
            // 
            this.breezahcheckbox.AutoSize = true;
            this.breezahcheckbox.Location = new System.Drawing.Point(13, 13);
            this.breezahcheckbox.Name = "breezahcheckbox";
            this.breezahcheckbox.Size = new System.Drawing.Size(70, 17);
            this.breezahcheckbox.TabIndex = 0;
            this.breezahcheckbox.Text = "BrEeZaH";
            this.breezahcheckbox.UseVisualStyleBackColor = true;
            // 
            // leetcheckbox
            // 
            this.leetcheckbox.AutoSize = true;
            this.leetcheckbox.Location = new System.Drawing.Point(13, 36);
            this.leetcheckbox.Name = "leetcheckbox";
            this.leetcheckbox.Size = new System.Drawing.Size(75, 17);
            this.leetcheckbox.TabIndex = 1;
            this.leetcheckbox.Text = "l33t sp34k";
            this.leetcheckbox.UseVisualStyleBackColor = true;
            // 
            // demontalkcheckbox
            // 
            this.demontalkcheckbox.AutoSize = true;
            this.demontalkcheckbox.Location = new System.Drawing.Point(13, 59);
            this.demontalkcheckbox.Name = "demontalkcheckbox";
            this.demontalkcheckbox.Size = new System.Drawing.Size(84, 17);
            this.demontalkcheckbox.TabIndex = 2;
            this.demontalkcheckbox.Text = "Demon Talk";
            this.demontalkcheckbox.UseVisualStyleBackColor = true;
            // 
            // capscheckbox
            // 
            this.capscheckbox.AutoSize = true;
            this.capscheckbox.Location = new System.Drawing.Point(13, 82);
            this.capscheckbox.Name = "capscheckbox";
            this.capscheckbox.Size = new System.Drawing.Size(87, 17);
            this.capscheckbox.TabIndex = 3;
            this.capscheckbox.Text = "Capslock On";
            this.capscheckbox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.overridekeyboardcheckbox);
            this.groupBox1.Location = new System.Drawing.Point(13, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(127, 45);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Keyboard";
            // 
            // overridekeyboardcheckbox
            // 
            this.overridekeyboardcheckbox.AutoSize = true;
            this.overridekeyboardcheckbox.Enabled = false;
            this.overridekeyboardcheckbox.Location = new System.Drawing.Point(6, 19);
            this.overridekeyboardcheckbox.Name = "overridekeyboardcheckbox";
            this.overridekeyboardcheckbox.Size = new System.Drawing.Size(114, 17);
            this.overridekeyboardcheckbox.TabIndex = 4;
            this.overridekeyboardcheckbox.Text = "Override Keyboard";
            this.overridekeyboardcheckbox.UseVisualStyleBackColor = true;
            // 
            // changeordercheckbox
            // 
            this.changeordercheckbox.AutoSize = true;
            this.changeordercheckbox.Location = new System.Drawing.Point(12, 103);
            this.changeordercheckbox.Name = "changeordercheckbox";
            this.changeordercheckbox.Size = new System.Drawing.Size(92, 17);
            this.changeordercheckbox.TabIndex = 5;
            this.changeordercheckbox.Text = "Change Order";
            this.changeordercheckbox.UseVisualStyleBackColor = true;
            // 
            // derp
            // 
            this.derp.AutoSize = true;
            this.derp.Checked = true;
            this.derp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.derp.Location = new System.Drawing.Point(13, 126);
            this.derp.Name = "derp";
            this.derp.Size = new System.Drawing.Size(49, 17);
            this.derp.TabIndex = 6;
            this.derp.Text = "Derp";
            this.derp.UseVisualStyleBackColor = true;
            // 
            // OptionsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(148, 210);
            this.Controls.Add(this.derp);
            this.Controls.Add(this.changeordercheckbox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.capscheckbox);
            this.Controls.Add(this.demontalkcheckbox);
            this.Controls.Add(this.leetcheckbox);
            this.Controls.Add(this.breezahcheckbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OptionsWindow";
            this.Text = "options";
            this.Load += new System.EventHandler(this.OptionsWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.CheckBox breezahcheckbox;
        public System.Windows.Forms.CheckBox leetcheckbox;
        public System.Windows.Forms.CheckBox demontalkcheckbox;
        public System.Windows.Forms.CheckBox capscheckbox;
        public System.Windows.Forms.CheckBox overridekeyboardcheckbox;
        public System.Windows.Forms.CheckBox changeordercheckbox;
        public System.Windows.Forms.CheckBox derp;
    }
}